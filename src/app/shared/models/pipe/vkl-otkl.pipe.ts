import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'vklOtkl'
})
export class VklOtklPipe implements PipeTransform {
  private vkl: any = {
    false: {name: 'Отключено'},
    true: {name: 'Включено'},
  };

  transform(value: string): string {
    return this.vkl[value].name;
  }

}
