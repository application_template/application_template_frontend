import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import {AuthorityDirective} from './directive/authority.directive';


@NgModule({
  declarations: [AuthorityDirective],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports: [
   AuthorityDirective
  ]
})
export class SharedModule { }
