import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {UserModule} from './user/user.module';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {
    path: 'userList',
    loadChildren: () => import('./user/user.module').then(m => m.UserModule)
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes),
    CommonModule, UserModule, NgbPaginationModule
  ]
})
export class EntitiesModule { }
