import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { FilterUserComponent } from './filter-user/filter-user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {AdminModule} from '../../admin/admin.module';
import {RolesNamePipe} from '../../shared/models/pipe/roles-name.pipe';
import {RolesPipe} from '../../shared/models/pipe/roles.pipe';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [UserComponent, FilterUserComponent, CreateUserComponent, RolesNamePipe, RolesPipe],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    NgMultiSelectDropDownModule,
    ReactiveFormsModule,
    AdminModule,
    NgbPaginationModule,
    SharedModule
  ]
})
export class UserModule { }
