import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {
  private router: Router;

  constructor(public authService: AuthService, router: Router) {
    this.router = router;
  }

  // tslint:disable-next-line:max-line-length
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log('сработал guard');
    const authorities = route.data['authorities'];
    console.log(authorities);
    if (this.authService.isAuth()) {
      if (!authorities) {
        return true;
      } else {
        return this.authService.isHasAuthority(authorities);
      }
    } else {
      this.router.navigate(['']);
      return false;
    }
  }

  // tslint:disable-next-line:max-line-length
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log('сработал guard');
    const authorities = childRoute.data['authorities'];
    if (this.authService.isAuth()) {
      if (!authorities) {
        return true;
      } else {
        return this.authService.isHasAuthority(authorities);
      }
    } else {
      this.router.navigate(['']);
      return false;
    }
  }

}
