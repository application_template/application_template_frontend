import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {AuthToken} from '../../shared/models/auth-token.model';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {User} from '../../shared/models/user-model';
import {Observable} from 'rxjs';
import {Role} from '../../shared/models/role-model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit, OnDestroy {

  url: string = GLOBAL_URL + '/api/auth';
  private principal: AuthToken = null;
  private currentRoles: Role[];
  route: Router;

  constructor(private http: HttpClient, route: Router) {
    this.route = route;
    if (this.isAuth()) {
      this.getAuthority().subscribe(res => {
        this.currentRoles = res.body;
      });
    }
  }

  ngOnDestroy(): void {
    this.clearJWTToken();
  }

  signIn(user: User): Observable<HttpResponse<AuthToken>> {
    return this.http.post<AuthToken>(this.url, user, {observe: 'response'});
  }

  registry(url: string): Observable<HttpResponse<User>> {
    return this.http.post<User>(this.url + '/registry', url, {observe: 'response'});
  }

  getAuthority(): Observable<HttpResponse<Role[]>> {
    return this.http.get<Role[]>(this.url + '/roles/', {
      headers: {Authorization: `Bearer ${this.getCurrentToken()}`},
      observe: 'response'
    });
  }

  isAuth() {
    return sessionStorage.getItem('successToken') != null;
  }

  /**
   * получить текущий токен
   */
  getCurrentToken(): string {
    return sessionStorage.getItem('successToken');
  }

  getCurrentLogin() {
    return sessionStorage.getItem('currentLogin');
  }

  setPrincipal(auth: AuthToken) {
    this.principal = auth;
    sessionStorage.setItem('currentLogin', auth.username);
    sessionStorage.setItem('successToken', auth.jwtToken);
    this.getAuthority().subscribe(res => {
      this.currentRoles = res.body;
    }, error => this.logout());
  }

  /**
   * очиска сессии
   */
  clearJWTToken() {
    this.principal = null;
    sessionStorage.clear();
  }

  isHasAnyAuthority(current: Role[], authorityes: string[] | string): boolean {
    let res = false;
    if (!this.isAuth()) {
      return false;
    }
    console.log('current = ');
    console.log(current);
    console.log('authorityes');
    console.log(authorityes);
    if (!Array.isArray(authorityes)) {
      authorityes = [authorityes];
    }
    current.forEach(auth => {
      if (authorityes.includes(auth.name)
      ) {
        res = true;
      }
    });
    console.log('вернуть = ' + res);
    return res;
  }

  isHasAuthority(authorityes: string[] | string): boolean {
    return this.isHasAnyAuthority(this.currentRoles, authorityes);
  }

  ngOnInit(): void {

  }

  logout() {
    this.clearJWTToken();
    this.route.navigate(['']);
  }

  /**
   *
   */
  subscribeEventRefresh() {
  }
}
